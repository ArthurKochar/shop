<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="ember-app/config/environment" content="%7B%22modulePrefix%22%3A%22ember-app%22%2C%22environment%22%3A%22development%22%2C%22rootURL%22%3A%22/%22%2C%22locationType%22%3A%22auto%22%2C%22EmberENV%22%3A%7B%22FEATURES%22%3A%7B%7D%7D%2C%22APP%22%3A%7B%22name%22%3A%22ember-app%22%2C%22version%22%3A%220.0.0+0dd8a4b0%22%7D%2C%22exportApplicationGlobal%22%3Atrue%7D" />
        <title>Shop</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://d2n844f18s487r.cloudfront.net/modules/templates/shared/styles/font-awesome-fc8e6f7946.css">

        <!-- Styles -->
        
        
        <link rel="stylesheet" type="text/css" href="/ember_app/assets/vendor.css">        
        <link rel="stylesheet" type="text/css" href="/ember_app/assets/ember-app.css">
                
    </head>
    <body>
        
        <script type="text/javascript" src="/ember_app/assets/vendor.js"></script>
        <script type="text/javascript" src="/ember_app/assets/ember-app.js"></script>
    </body>
</html>