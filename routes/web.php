<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('welcome');
});
$app->get('shops','ShopController@index');
$app->post('shops','ShopController@addShop');
$app->delete('shops/{id}','ShopController@deleteShop');
$app->get('shops/{shop_id}','ShopController@show');
$app->put('shops/{id}','ShopController@update');
$app->get('/shop/{shop_id}', function () use ($app) {
    return view('welcome');
});
$app->get('/shop-items/{shop_id}', function () use ($app) {
    return view('welcome');
});
$app->get('/item/{id}', function () use ($app) {
    return view('welcome');
});
$app->post('items','ItemController@addItem');
$app->delete('items/{id}','ItemController@deleteItem');
$app->get('items/{id}','ItemController@show');
$app->put('items/{id}','ItemController@update');

