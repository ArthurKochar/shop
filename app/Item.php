<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Item extends Model 
{
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','quantity','price','shop_id'
    ];
    public function shop()
    {
        return $this->belongsTo('App\Shop');
    }

    
}
