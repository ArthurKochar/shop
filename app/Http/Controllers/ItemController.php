<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Shop;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Item $item)
    {
        $this->item = $item;
        
    }
    public function index(Request $request , $shop_id)
    {
        if($request->ajax())
        {
            $data = Shop::whereId($shop_id)->with('items')->get();
            return response()->json(['items' => $data]);
        }
        return view('welcome');
    }
    public function addItem(Request $request)
    {
        $this->validate( $request, [
            'item.name' => 'required',
            'item.quantity' => 'required|numeric',
            'item.price' => 'required|numeric',
        ]);
        $inputs = $request->only('item')['item'];
        $item = [
                    'name'=>$inputs['name'],
                    'shop_id' => $inputs['shop_id'],
                    'quantity' => $inputs['quantity'],
                    'price' => $inputs['price']
                ];
       
        $data =  $this->item ->create($item);
        return response()->json(['item' => $data]);
    }
    public function deleteItem($id)
    {
        $item = $this->item->find($id);    
        $item->delete();
        return response()->json('');
       

    }
    public function show(Request $request , $id)
    {
        if($request->ajax())
        {
            $data = $this->item->whereId($id)->get();
            return response()->json(['items' => $data]);
        }
        return view('welcome');
    }
    public function update(Request $request , $id)
    {
        $this->validate( $request, [
            'item.name' => 'required',
            'item.quantity' => 'required|numeric',
            'item.price' => 'required|numeric',
        ]);
        $inputs = $request->only('item')['item'];
        $item = $this->item->find($id);
            $item->name =  $inputs['name'];
            $item->quantity =  $inputs['quantity'];
            $item->price =  $inputs['price'];
        $item->save();

        return response()->json(['items' => $item]);

    }

    
}
