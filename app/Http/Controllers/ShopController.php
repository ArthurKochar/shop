<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;

class ShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
        
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->shop->with('items')->get();
            return ['shops' => $data];
        }
        return redirect('/');    
    }
    public function addShop(Request $request)
    {

        $inputs = $request->all()['shop'];
        $this->validate( $request, [
            'shop.name' => 'required',
        ]);
        $data = $this->shop->create($inputs);
        return response()->json(['shop' => $data]);
    }
    public function deleteShop($id)
    {
        $shop = $this->shop->find($id);    
        $shop->delete();
        return response()->json('');
       

    }
    public function show(Request $request , $shop_id)
    {
        if($request->ajax())
        {
            $data = Shop::whereId($shop_id)->with('items')->get();
            return response()->json(['shops' => $data]);
        }
        return view('welcome');
    }
    public function update(Request $request , $id)
    {
        
        $inputs = $request->only('shop')['shop'];
        $this->validate( $request, [
            'shop.name' => 'required',
        ]);

        $shop = $this->shop->find($id);

        $shop->name =  $inputs['name'];

        $shop->save();

        return response()->json(['shop' => $shop]);

    }
    

    
}
