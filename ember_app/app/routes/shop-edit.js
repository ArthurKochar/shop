import Ember from 'ember';

export default Ember.Route.extend({
	model(params) {
		return this.get('store').findRecord('shop',params.id);
    	
  	},
  	actions: {
		editShop:function(model){
			model.save().then(this.transitionTo('shops'));
			this.transitionTo('shops');
		}
		
	}
});
