import Ember from 'ember';

export default Ember.Route.extend({
	model(params) {
		return this.get('store').findRecord('item',params.id);
  },actions:{
  		editItem(id,model){
  			 model.save().then(this.transitionTo('/shop-items/'+id));
		}
  	}
});
