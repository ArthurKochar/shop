import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('shops');
  this.route('shop-items',{ path: '/shop-items/:shop_id' });
  this.route('shop-edit',{ path: '/shop/:id' });
  this.route('item-edit',{ path: '/item/:id' });
});

export default Router;
