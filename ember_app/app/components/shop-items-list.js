import Ember from 'ember';

export default Ember.Component.extend({
	addItem: "addItem",
	deleteItem:"deleteItem",
    actions: {
        addItem: function(id,data){
            this.sendAction("addItem", id,data);
            data.set('name', '');
            data.set('quantity', '');
            data.set('price', '');
        },
        deleteItem(id){
            this.sendAction("deleteItem",id);
        }
    }
});
