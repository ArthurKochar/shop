import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
	name : DS.attr('string'),
	items: DS.hasMany('item'),
	itemPriceArray: Ember.computed.mapBy('items', 'price'),
  	totalCount: Ember.computed.sum('itemPriceArray')
});
