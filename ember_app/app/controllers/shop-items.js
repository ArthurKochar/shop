import Ember from 'ember';

export default Ember.Controller.extend({
	actions:{
		addItem(id,data){
              let model = this.get('store').peekRecord('shop', id);
              let item = this.get('store').createRecord('item', {
                    name: data.name,
                    shop_id:id,
                    quantity:data.quantity,
                    price:data.price,
              });
              item.save().then(function(response) {
                  model.get('items').pushObject(item);
              },function(response) {
                  if(response.errors){
                      item.destroyRecord();
                      alert(':(');
                  }

              });


            },
        deleteItem(id){
              this.store.findRecord('item', id, { backgroundReload: false }).then(function(item) {
                    item.destroyRecord();
              });
        }
	}
});
