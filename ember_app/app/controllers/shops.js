import Ember from 'ember';

export default Ember.Controller.extend({
	actions:{
		addShop(shopName) {
			let shop = this.store.createRecord('shop',{
				name : shopName,
			});
			shop.save().then(function(response) {

            },function(response) {
				if(response.errors){
                    shop.destroyRecord();
                    alert(':(');
                }

            });
            this.set('shopName', '');

		},
		deleteShop(id) {
			this.store.findRecord('shop', id, { backgroundReload: false }).then(function(shop) {
  				shop.destroyRecord(); 
			});
		}
		
	}
});
